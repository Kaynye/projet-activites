<?php
	session_start();
?>
<html>
<head>
<meta charset='utf-8' /><title>Agenda</title>
<link href='fullcalendar-3.4.0/fullcalendar.min.css' rel='stylesheet' />
<link href='fullcalendar-3.4.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='fullcalendar-3.4.0/lib/moment.min.js'></script>
<script src='fullcalendar-3.4.0/lib/jquery.min.js'></script>
<script src='fullcalendar-3.4.0/lib/jquery-ui.min.js'></script>
<script src='fullcalendar-3.4.0/fullcalendar.min.js'></script>
<script src='fullcalendar-3.4.0/locale/fr.js'></script>
<script>

	$(document).ready(function() {


		/* initialize the external events
		-----------------------------------------------------------------*/

		var event = $('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
				
			});
			

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		var calendar = $('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'agendaWeek,agendaDay'
			},
			validRange: {
				start: '2017-05-01',
				
			},
			defaultView: 'agendaDay',
			minTime :"08:00:00",
			maxTime :"20:00:00",
			lang:'fr',
			allDaySlot: false,
			height: 350,
			defaultTimedEventDuration: "01:00:00",
			slotDuration: '01:00:00',
			editable: true,
			droppable: true,
			events: 'events.php',
			drop: function(date) {
				 var oEventObject = $(this).data('event');
				 var title = oEventObject.title;
				 console.log(oEventObject.start);
				console.log(date);
				start = new Date( $.fullCalendar.formatDate(date, "YYYY-MM-DD HH:mm"));//je recupere la date
				end=new Date (start);
				end.setHours(start.getHours()+1);//j'initialiser la date de fin
				console.log(start.getHours());
				console.log(end.getHours());
				console.log(date);
				color= coloring(title);
				$.ajax({
					 url: 'add_event.php',
					 data: '&idUser='+'<?php echo $_SESSION['id'];?>'+'&title='+ title+'&start='+ start +'&end='+ end+'&color='+ color,
					 type: "POST",
					 success: function () {
						  location.reload();
						  //alert('aa');	
						  //$('calendar').load('calendar.php calendar',function(){alert('ok');});
						}
			});
					 
					 //calendar.fullCalendar('renderEvent',{
					 //title: title,
					 //start: start,
					 //end: end,
					 //allDay: allDay
					 //},
					 //true // make the event "stick"
					 //);
			
			},		
			//eventClick: function(event) {
			//alert('supr');
			//},
			eventDragStop: function(event,jsEvent) {

				var trashEl = jQuery('#calendarTrash');
				var ofs = trashEl.offset();

				var x1 = ofs.left;
				var x2 = ofs.left + trashEl.outerWidth(true);
				var y1 = ofs.top;
				var y2 = ofs.top + trashEl.outerHeight(true);

				if (jsEvent.pageX >= x1 && jsEvent.pageX<= x2 &&
					jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
					//alert(event.id);
					$.ajax({	
						url: 'remove_event.php',
						data: 'id=' + event.id,
						type: "POST",
						success: function () {
						  $('#calendar').fullCalendar('removeEvents', event.id);
						}
					});
				}
			},
			eventResize:function(event){
				start = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm");
				end = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm");
				$.ajax({
					url: 'update_event.php',
					 data: '&id='+event.id+'&title='+ event.title+'&start='+ start +'&end='+ end,
					 type: "POST"
				})
				
			},
			eventDrop:function(event){
				start = $.fullCalendar.formatDate(event.start, "YYYY-MM-DD HH:mm");
				end = $.fullCalendar.formatDate(event.end, "YYYY-MM-DD HH:mm");
				$.ajax({
					url: 'update_event.php',
					 data: '&id='+event.id+'&title='+ event.title+'&start='+ start +'&end='+ end,
					 type: "POST"
				})
				
			},
			eventOverlap: function(stillEvent, movingEvent) {
				return false;
			}
		});
		
	function coloring(titre){
		if (titre=='Java'){return '#2c4061';}
		else if (titre== 'Python'){return '#2e9059';}
		else if (titre== 'Anglais'){return '#8265a1';}
		else if (titre== 'Repos'){return '#db4e23';}
		else if (titre== 'Café'){return '#f17922';}
		else {return '#5e574a';}
		}

	});

</script>
<style>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
	}
		
	#wrap {
		width: 1100px;
		margin: 0 auto;
	}
		
	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}
		
	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}
		
	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}
		
	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}
		
	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: right;
		width: 800px;<!-- a la base a 900 -->
		
	}

</style>
</head>
<body>
	<div id='wrap'>

		<div id='external-events'>
			<h4>Activité de <?php echo $_SESSION['nomUser'];?></h4>
			<div class='fc-event' style="background-color:	#2c4061; border-color: #2c4061">Java</div>
			<div class='fc-event' style="background-color:	#2e9059; border-color: #2e9059">Python</div>
			<div class='fc-event' style="background-color:	#8265a1; border-color: #8265a1">Anglais</div>
			<div class='fc-event' style="background-color:	#db4e23; border-color: #db4e23">Repos</div>
			<div class='fc-event' style="background-color:	#f17922; border-color: #f17922">Café</div>
			<div class='fc-event' style="background-color:	#5e574a; border-color: #5e574a">PHP</div>
			<p>
				<input type='checkbox' hidden id='drop-remove' />
				<label for='drop-remove' hidden >enlever</label>
			</p>
			<form>
			<input type="button" value="Deconnexion" onclick="window.location.href='deconnection.php'"/>
			</form>

		</div>

		<div id='calendar'></div>
		<div id="calendarTrash" class="calendar-trash"><img src="trash.png" width=80 height=80/></div>

		<div style='clear:both'></div>

	</div>
</body>
</html>
