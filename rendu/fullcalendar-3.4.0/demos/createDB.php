<?php
date_default_timezone_set('Europe/Paris');
try{

 $file_db = new PDO('sqlite:utilisateurs.sqlite3');
 $file_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 //$file_db->exec("DROP TABLE  IF EXISTS utilisateurs;");
 //$file_db->exec("DROP TABLE IF EXISTS evenement;");

 $file_db->exec("CREATE TABLE IF NOT EXISTS utilisateurs(
        id INTEGER PRIMARY KEY,
        nom TEXT,
        prenom TEXT,
        adresse TEXT,
        age INTEGER,
        mail TEXT,
        pseudo TEXT,
        motDePasse TEXT)");

 $file_db->exec("CREATE TABLE IF NOT EXISTS evenement(
         id INTEGER PRIMARY KEY,
         idUser INTEGER, 
         title TEXT,
         start TIME,
         end TIME,
         FOREIGN KEY (idUser) REFERENCES utilisateurs(id))");



$file_db = null;
}catch(PDOException $e){
echo $e->getMessage();

}
?>
